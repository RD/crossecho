import logging
import argparse
from time import sleep
from lib.opc_interface import define_get_crosslet_statistics_parameters, setup_server_metadata, register_namespace,\
    define_rcu_object


# ----------------------------------MAIN CODE LOGIC
def setup_logging():
    """
    Setup the logging system
    """
    logging.basicConfig(
        format='%(asctime)s - %(levelname)s - %(name)s: %(message)s',
        datefmt="%m/%d/%Y %I:%M:%S %p",
        level=logging.INFO)


def init():
    """
    Init phase of the program
    """
    setup_logging()


def setup_command_argument_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Record cross correlation statistics and offers it through OPC UA")

    parser.add_argument('--port', help='tcp port', type=int, default=55555)
    return parser


def main():
    init()
    parser = setup_command_argument_parser()
    arguments = parser.parse_args()
    server = setup_server_metadata(port=arguments.port)
    namespace_idx = register_namespace(server)
    rcu_obj = define_rcu_object(server, namespace_idx)
    define_get_crosslet_statistics_parameters(rcu_obj, namespace_idx)

    try:
        # Start the server.
        server.start()
        while True:
            sleep(1)
    except Exception as e:
        logging.exception(e)
    finally:
        # close connection, remove subcsriptions, etc
        server.stop()

if __name__=='__main__':
    main()