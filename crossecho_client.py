import logging
import argparse
from lib.opc_interface import connect_to_server, DEFAULT_URI
import numpy


# ----------------------------------MAIN CODE LOGIC
def setup_logging():
    """
    Setup the logging system
    """
    logging.basicConfig(
        format='%(asctime)s - %(levelname)s - %(name)s: %(message)s',
        datefmt="%m/%d/%Y %I:%M:%S %p",
        level=logging.INFO)


def init():
    """
    Init phase of the program
    """
    setup_logging()


def setup_command_argument_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Read cross correlation statistics and offers it through OPC UA")

    parser.add_argument('--url', help='url to connect to', type=str, default='localhost')
    parser.add_argument('--port', help='url to connect to', type=int, default=55555)
    parser.add_argument('--path', help='sub path', type=str, default='/LOFAR2.0/CROSSECHO')

    return parser


def main():
    init()
    parser = setup_command_argument_parser()
    arguments = parser.parse_args()
    client = connect_to_server(arguments.url, arguments.port)
    objects = client.get_objects_node()
    idx = client.get_namespace_index(DEFAULT_URI)

    # Now getting a variable node using its browse path
    obj = client.get_root_node().get_child(["0:Objects",
                                            "{}:StationMetrics".format(idx),
                                            "{}:RCU".format(idx)])
    result = obj.call_method("{}:record_cross".format(idx), 100, 2)
    print("Timestamp is ", result[0])
    print("Crosscorrelations are ", numpy.array(result[1]))
    print("RCU modes are", numpy.array(result[2]))

    client.close_session()
    client.close_secure_channel()


if __name__ == '__main__':
    main()
