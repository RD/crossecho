import logging

from opcua import ua, uamethod, Server, Client
from .rspctl_probe import produce_xcstatistics, set_xcsubband
import numpy

DEFAULT_URI = "http://lofar.eu"
NUM_RCU = 96
# method to be exposed through server
# uses a decorator to automatically convert to and from variants
@uamethod
def get_crosslet_statistics(parent, subband, integration_time):
    from datetime import datetime
    print("Ask to record subband {} with integration time {}".format(subband,
                                                                     integration_time))
    set_xcsubband(subband)

    res = produce_xcstatistics(integration_time=integration_time, duration=integration_time)
    data = res['data']
    time_slots = int(len(data) / NUM_RCU / NUM_RCU)
    crosslets_complex = data.reshape((time_slots, NUM_RCU, NUM_RCU))
    crosslets = numpy.zeros((2, NUM_RCU, NUM_RCU), dtype=float)
    crosslets[0, :, :] = crosslets_complex.real
    crosslets[1, :, :] = crosslets_complex.imag

    crosslets = crosslets.tolist()
    timestamp = datetime.utcnow()
    rcu = res['rcus']
    rcus_mode = [rcu[i]["mode"] for i in rcu]

    return timestamp, crosslets, rcus_mode


def define_get_crosslet_statistics_parameters(object, idx):
    subband = ua.Argument()
    subband.Name = "subband"
    subband.DataType = ua.NodeId(ua.ObjectIds.Int16)
    subband.ValueRank = -1
    subband.ArrayDimensions = []
    subband.Description = ua.LocalizedText("subband to use to collect the crosslet statistics")

    integration_time = ua.Argument()
    integration_time.Name = "integration_time"
    integration_time.DataType = ua.NodeId(ua.ObjectIds.Int16)
    integration_time.ValueRank = -1
    integration_time.ArrayDimensions = []
    integration_time.Description = ua.LocalizedText("integration time")

    cross = ua.Argument()
    cross.Name = "crosslet_statistics"
    cross.DataType = ua.NodeId(ua.ObjectIds.Float)
    cross.ValueRank = 3
    cross.ArrayDimensions = [96, 96, 2]
    cross.Description = ua.LocalizedText("crosscorrelation statistics")

    rcusmode = ua.Argument()
    rcusmode.Name = "rcus_mode"
    rcusmode.DataType = ua.NodeId(ua.ObjectIds.UInt16)
    rcusmode.ValueRank = 1
    rcusmode.ArrayDimensions = [96]
    rcusmode.Description = ua.LocalizedText("RCUs mode")

    timestamp = ua.Argument()
    timestamp.Name = "timestamp"
    timestamp.DataType = ua.NodeId(ua.ObjectIds.DateTime)
    timestamp.ValueRank = -1
    timestamp.ArrayDimensions = []
    timestamp.Description = ua.LocalizedText("timestamp")

    return object.add_method(idx, "record_cross", get_crosslet_statistics,
                             [subband, integration_time],
                             [timestamp, cross, rcusmode])


def connect_to_server(url, port):
    client = Client("opc.tcp://{}:{}/".format(url, port), timeout=1000)
    client.connect()
    client.load_type_definitions()
    return client


def setup_server_metadata(server=None, port=55555):
    if server is None:
        server = Server()
    server.set_endpoint("opc.tcp://0.0.0.0:%d/LOFAR2.0/CROSSECHO" % port)
    server.set_server_name("LOFAR2.0 Crosscorrelations statistic echo server (CROSSECHO)")
    return server


def register_namespace(server, uri=DEFAULT_URI):
    idx = server.register_namespace(uri=uri)
    return idx


def define_rcu_object(server, idx):
    # get Objects node, this is where we should put our custom stuff
    objects = server.get_objects_node()

    # populating our address space
    station_metric_folder = objects.add_folder(idx, "StationMetrics")
    rcu = station_metric_folder.add_object(idx, "RCU")
    return rcu

